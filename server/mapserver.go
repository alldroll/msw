package main

import (
	"flag"
	"log"
	"net/http"
	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", ":8080", "ws service address")

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}


func serveWs(w http.ResponseWriter, r *http.Request) {
	_, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("error")
		return
	}
	log.Print("server")
}

func main() {
	flag.Parse()
	http.HandleFunc("/ws", serveWs)
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
